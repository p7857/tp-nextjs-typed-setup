> The ´degit´ packages actually seems to have some unfixed issues cloning from gitlab. If it doesn't work, you have to clone it manually.

# nextjs typed setup
> v0.1.0

Initialize a new production ready Node Project. ReactJS powered by the NEXTJS Framework.

## Requirements: 
* [NodeJS ^16.14](https://nodejs.org/en/)
* NPM ^8.3.1

## Features
* typescript
* sass/scss
* vanilla-js with typescript support
* Prettier
* autoprefixer
* nextjs

## Get Started

Get started using [degit](https://github.com/Rich-Harris/degit)
```
# install degit
$ npm i -g degit

# clone this repository
$ degit gitlab:mrdodoseen/tp-nextjs-typed-setup

$ cd tp-nextjs-typed-setup
$ cd app

# install dependencies
$ npm i

```

## Plugins:


## SASS/SCSS


